﻿using GetBEE.TechnicalAssignment.Data.Data.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace GetBEE.TechnicalAssignment.Data.DataAccessLayer
{
    public class UserDataLayer : IUserDataLayer
    {
        private readonly GetBEEContext _getBEEContext;

        public UserDataLayer(GetBEEContext getBEEContext)
        {
            _getBEEContext = getBEEContext;
        }

        public UserDetail Get(string Id)
        {
            return (from user in _getBEEContext.UserDetail
                    join aspnetUsers in _getBEEContext.AspNetUsers on user.UserId equals aspnetUsers.Id
                    where user.UserId == Id
                    select user)
                   .FirstOrDefault();
        }

        public List<UserDetail> GetAll()
        {
            return (from user in _getBEEContext.UserDetail
                    join aspnetUsers in _getBEEContext.AspNetUsers on user.UserId equals aspnetUsers.Id
                    where user.IsActive == true
                    select user)
                   .ToList();
        }

        public List<UserDetail> GetMany(string searchParameter)
        {
            throw new NotImplementedException();
        }

        public UserDetail SaveUserDetail(UserDetail userDetail)
        {
            var user = _getBEEContext.UserDetail.Where(x => x.UserId == userDetail.UserId).FirstOrDefault();

            if (user == null)
            {
                _getBEEContext.UserDetail.Add(userDetail);
            }
            else
            {
                user.DateOfBirth = userDetail.DateOfBirth;
                user.FirstName = userDetail.FirstName;
                user.LastName = userDetail.LastName;
                user.UserDescription = userDetail.UserDescription;
                user.Occupation = userDetail.Occupation;
                user.IsActive = userDetail.IsActive;
                _getBEEContext.Attach(user);
            }

            _getBEEContext.SaveChanges();
            return userDetail;
        }
    }
}
