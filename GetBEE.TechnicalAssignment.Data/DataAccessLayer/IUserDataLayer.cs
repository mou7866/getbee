﻿using GetBEE.TechnicalAssignment.Data.Data.Entity;
using System.Collections.Generic;

namespace GetBEE.TechnicalAssignment.Data.DataAccessLayer
{
    public interface IUserDataLayer
    {
        UserDetail Get(string Id);

        List<UserDetail> GetMany(string searchParameter);

        UserDetail SaveUserDetail(UserDetail userDetail);

        List<UserDetail> GetAll();
    }
}
