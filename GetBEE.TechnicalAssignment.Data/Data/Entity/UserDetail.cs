﻿using System;
using System.Collections.Generic;

namespace GetBEE.TechnicalAssignment.Data.Data.Entity
{
    public partial class UserDetail
    {
        public string UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Occupation { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string UserDescription { get; set; }
        public bool? IsActive { get; set; }

        public virtual AspNetUsers User { get; set; }
    }
}
