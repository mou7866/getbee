﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GetBEE.TechnicalAssignment.DataTransferObject.User
{
    public class UserDetailDTO
    {
        public string Id { get; set; }

        public string EmailAddress { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string FirstName { get; set; }
        public bool? IsActive { get; set; }
        public string LastName { get; set; }
        public string Occupation { get; set; }
        public string UserDescription { get; set; }
        public string UserId { get; set; }
    }
}
