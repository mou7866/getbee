﻿using System;
using System.Collections.Generic;
using System.Text;
using GetBEE.TechnicalAssignment.Data.Data.Entity;
using GetBEE.TechnicalAssignment.Data.DataAccessLayer;
using GetBEE.TechnicalAssignment.DataTransferObject.User;

namespace GetBEE.TechnicalAssignment.Business
{
    public class UserService : IUserService
    {
        private readonly IUserDataLayer _userDataLayer;
        public UserService(IUserDataLayer userDataLayer)
        {
            _userDataLayer = userDataLayer;
        }

        public UserDetailDTO GetUserDetail(string userId)
        {
            UserDetailDTO detailDTO = new UserDetailDTO()
            {
                Id = userId
            };

            var user = _userDataLayer.Get(userId);

            if (user != null)
            {
                detailDTO.DateOfBirth = user.DateOfBirth;
                detailDTO.FirstName = user.FirstName;
                detailDTO.IsActive = user.IsActive;
                detailDTO.LastName = user.LastName;
                detailDTO.Occupation = user.Occupation;
                detailDTO.UserDescription = user.UserDescription;
                detailDTO.UserId = user.UserId;
            }

            return detailDTO;
        }

        public List<UserDetailDTO> GetUsers()
        {
            List<UserDetailDTO> userDetails = new List<UserDetailDTO>();

            var userData = _userDataLayer.GetAll();

            if (userData != null)
            {
                foreach (var user in userData)
                {
                    userDetails.Add(new UserDetailDTO()
                    {
                        DateOfBirth = user.DateOfBirth,
                        FirstName = user.FirstName,
                        IsActive = user.IsActive,
                        LastName = user.LastName,
                        Occupation = user.Occupation,
                        UserDescription = user.UserDescription,
                        UserId = user.UserId
                    });
                }
            }

            return userDetails;
        }

        public UserDetailDTO SaveUser(UserDetailDTO userDetailDTO)
        {
            UserDetail userDetail = new UserDetail()
            {
                DateOfBirth = userDetailDTO.DateOfBirth,
                FirstName = userDetailDTO.FirstName,
                IsActive = userDetailDTO.IsActive,
                LastName = userDetailDTO.LastName,
                Occupation = userDetailDTO.Occupation,
                UserDescription = userDetailDTO.UserDescription,
                UserId = userDetailDTO.UserId
            };

            _userDataLayer.SaveUserDetail(userDetail);
            return userDetailDTO;
        }

        public List<UserDetailDTO> SearchUsers(string searchParameter)
        {
            throw new NotImplementedException();
        }
    }
}
