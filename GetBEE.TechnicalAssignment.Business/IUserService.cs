﻿using GetBEE.TechnicalAssignment.DataTransferObject.User;
using System;
using System.Collections.Generic;
using System.Text;

namespace GetBEE.TechnicalAssignment.Business
{
    public interface IUserService
    {
        UserDetailDTO GetUserDetail(string userId);

        List<UserDetailDTO> SearchUsers(string searchParameter);

        UserDetailDTO SaveUser(UserDetailDTO userDetailDTO);

        List<UserDetailDTO> GetUsers();
    }
}
