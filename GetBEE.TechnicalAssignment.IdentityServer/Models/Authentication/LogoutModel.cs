﻿namespace GetBEE.TechnicalAssignment.DataTransferObject.Authentication
{
    public class LogoutModel
    {
        public string LogoutId { get; set; }

        public bool ShowLogoutPrompt { get; set; } = true;
    }
}
