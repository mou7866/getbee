namespace GetBEE.TechnicalAssignment.DataTransferObject.Authentication
{
    public class RedirectViewModel
    {
        public string RedirectUrl { get; set; }
    }
}