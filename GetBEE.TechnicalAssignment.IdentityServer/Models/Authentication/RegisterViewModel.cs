﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GetBEE.TechnicalAssignment.DataTransferObject.Authentication
{
    public class RegisterViewModel : ApplicationUser
    {
        public string ConfirmPassword { get; set; }
        public string ReturnUrl { get; set; }
    }
}
