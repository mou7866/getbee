﻿using Microsoft.AspNetCore.Identity;

namespace GetBEE.TechnicalAssignment.DataTransferObject.Authentication
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public string Password { get; set; }
    }
}
