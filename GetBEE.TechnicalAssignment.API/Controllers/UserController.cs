﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GetBEE.TechnicalAssignment.Business;
using GetBEE.TechnicalAssignment.DataTransferObject.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GetBEE.TechnicalAssignment.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet("{userId}")]
        public IActionResult Get(string userId)
        {
            return Ok(_userService.GetUserDetail(userId));
        }

        [HttpPost()]
        public IActionResult Post(UserDetailDTO user)
        {
            return Ok(_userService.SaveUser(user));
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_userService.GetUsers());
        }
    }
}