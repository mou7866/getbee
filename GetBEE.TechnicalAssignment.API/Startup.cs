﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GetBEE.TechnicalAssignment.Business;
using GetBEE.TechnicalAssignment.Data.Data.Entity;
using GetBEE.TechnicalAssignment.Data.DataAccessLayer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace GetBEE.TechnicalAssignment.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthorization();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddAuthentication("Bearer")
                .AddJwtBearer("Bearer", options =>
                {
                    options.Authority = "http://localhost:5000";
                    options.RequireHttpsMetadata = false;

                    options.Audience = "api1";
                });

            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IUserDataLayer, UserDataLayer>();
            services.AddDbContext<GetBEEContext>(options => options.UseSqlServer(Configuration["ConnectionStrings:Users"], providerOptions => providerOptions.CommandTimeout(60)), ServiceLifetime.Transient, ServiceLifetime.Transient);

            services.AddCors(options =>
            {
                options.AddPolicy("default", policy =>
                {
                    policy.WithOrigins("http://localhost:5002")
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseCors("default");

            app.UseAuthentication();
            app.UseMvc();
        }
    }
}
