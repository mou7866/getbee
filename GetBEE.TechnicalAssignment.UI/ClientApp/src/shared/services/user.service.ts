import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';

@Injectable()
export class UserService {
    
  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<any[]>(environment.apiUrl + '/api/Values');
  }

  getUser(userId: any): any {
    return this.http.get<any>(environment.apiUrl + '/api/User/' + userId);
  }

  saveUser(user: any): any {
    return this.http.post<any>(environment.apiUrl + '/api/User/', user);
  }

  getAllUsers(): any {
    return this.http.get<any>(environment.apiUrl + '/api/User');
  }
  
}
