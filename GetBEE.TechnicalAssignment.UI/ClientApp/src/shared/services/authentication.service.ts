import { Injectable } from '@angular/core';
import { UserManager, UserManagerSettings, User, WebStorageStateStore } from 'oidc-client';
import { AuthenticationConfiguration } from '../helpers/authentication.configuration';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class AuthenticationService {

  private user: User = null;
  private manager = null;
  private userStore = new WebStorageStateStore({ store: window.localStorage });
  // Observable navItem source
  private _authNavStatusSource = new BehaviorSubject<boolean>(false);
  // Observable navItem stream
  authNavStatus$ = this._authNavStatusSource.asObservable();

  constructor(private authenticationConfiguration: AuthenticationConfiguration) {
    this.manager = new UserManager(authenticationConfiguration.getClientSettings());
    this.manager.getUser().then(user => {
      this.user = user;
      this._authNavStatusSource.next(this.isAuthenticated());
    });
  }

  isLoggedIn(): boolean {
    return this.user != null && !this.user.expired;
  }

  isAuthenticated(): boolean {
    return this.user != null && !this.user.expired;
  }

  getClaims(): any {
    return this.user.profile;
  }

  getAuthorizationHeaderValue(): string {
    return `${this.user.token_type} ${this.user.access_token}`;
  }

  startAuthentication(): Promise<void> {
    return this.manager.signinRedirect();
  }

  async completeAuthentication() {
    this.user = await this.manager.signinRedirectCallback();
    this._authNavStatusSource.next(this.isAuthenticated());
  }

  get name(): string {
    return this.user != null ? this.user.profile.name : '';
  }

  signout() {
    this.manager.signoutRedirect();
  }
}
