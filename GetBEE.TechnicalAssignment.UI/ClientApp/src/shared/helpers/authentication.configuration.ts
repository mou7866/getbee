import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Observable, Subject } from 'rxjs';
import { UserManagerSettings } from 'oidc-client';

@Injectable()
export class AuthenticationConfiguration {
  getClientSettings(): UserManagerSettings {
    return {
      authority: 'http://localhost:5000/',
      client_id: 'GetBEE',
      redirect_uri: 'http://localhost:5002/authentication-callback',
      post_logout_redirect_uri: 'http://localhost:5002/',
      response_type: "id_token token",
      scope: "openid profile api1",
      filterProtocolClaims: true,
      loadUserInfo: true
    };
  }
}
