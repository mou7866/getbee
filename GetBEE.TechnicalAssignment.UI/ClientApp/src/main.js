"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_dynamic_1 = require("@angular/platform-browser-dynamic");
var app_module_1 = require("./app/app.module");
var environment_1 = require("./environments/environment");
var http_1 = require("@angular/common/http");
var apiurl_interceptor_1 = require("./shared/helpers/apiurl.interceptor");
function getBaseUrl() {
    return document.getElementsByTagName('base')[0].href;
}
exports.getBaseUrl = getBaseUrl;
exports.API_URL = new core_1.InjectionToken('apiUrl');
var providers = [
    { provide: 'BASE_URL', useFactory: getBaseUrl, deps: [] },
    { provide: 'API_URL', useFactory: getBaseUrl, deps: [] },
    { provide: exports.API_URL, useValue: environment_1.environment.apiUrl },
    { provide: http_1.HTTP_INTERCEPTORS, useClass: apiurl_interceptor_1.ApiUrlInterceptor, multi: true, deps: [exports.API_URL] }
];
if (environment_1.environment.production) {
    core_1.enableProdMode();
}
platform_browser_dynamic_1.platformBrowserDynamic(providers).bootstrapModule(app_module_1.AppModule)
    .catch(function (err) { return console.log(err); });
//# sourceMappingURL=main.js.map