import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { AccountModule } from './account/account.module';
import { AuthenticationGuard } from '../shared/guards/authentication.guard';
import { AuthenticationCallbackComponent } from './authentication/authentication-callback/authenticationcallback.component';
import { AuthenticationService } from '../shared/services/authentication.service';
import { AuthenticationConfiguration } from '../shared/helpers/authentication.configuration';
import { JwtInterceptor } from '../shared/helpers/jwt.interceptor';
import { UserService } from '../shared/services/user.service';
import { LoaderInterceptorService } from '../shared/helpers/loader.interceptor';
import { LoaderService } from '../shared/services/loader.service';
import { LoaderComponent } from '../shared/component/loader.component';
import { UserSearchComponent } from './user-search/user-search.component';
import { UserProfileComponent } from './user-profile/user-profile.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    UserSearchComponent,
    UserProfileComponent,
    AuthenticationCallbackComponent,
    LoaderComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,    
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full', canActivate: [AuthenticationGuard] },
      { path: 'user-search', component: UserSearchComponent, canActivate: [AuthenticationGuard]},
      { path: 'user-profile/:userId', component: UserProfileComponent, canActivate: [AuthenticationGuard] },
      { path: 'authentication-callback', component: AuthenticationCallbackComponent }
    ]),
     AccountModule
  ],
  providers: [AuthenticationGuard, AuthenticationService, AuthenticationConfiguration, UserService, LoaderService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoaderInterceptorService,
      multi: true
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }
