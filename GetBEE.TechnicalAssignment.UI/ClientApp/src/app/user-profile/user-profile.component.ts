import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserService } from '../../shared/services/user.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'user-profile',
  templateUrl: './user-profile.component.html'
})
export class UserProfileComponent implements OnInit {

  user: any = null;
  userId: any | null;
  constructor(private route: ActivatedRoute,
    private userService: UserService) {
    
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.userId = params.get("userId");
    });

    if (this.userId != null && this.userId != "") {
      this.userService
        .getUser(this.userId)
        .subscribe(user => {
          this.user = user;
        });
    }
  }  
}

