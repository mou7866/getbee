import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../../shared/services/authentication.service';

@Component({
  selector: 'authentication-callback',
  templateUrl: './authenticationcallback.component.html'
})
export class AuthenticationCallbackComponent implements OnInit {

  constructor(private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.authenticationService.completeAuthentication();
  }

}
