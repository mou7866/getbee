import { Component } from '@angular/core';
import { UserService } from '../../shared/services/user.service';
import { AuthenticationService } from '../../shared/services/authentication.service';

@Component({
  selector: 'user-search',
  templateUrl: './user-search.component.html'
})
export class UserSearchComponent {

  userDetails: any[] = [];
  userSaved: boolean = false;
  constructor(private userService: UserService,
    private authenticationService: AuthenticationService) {
    this.userService
      .getAllUsers()
      .subscribe(userData => {
        this.userDetails = userData;
      });
  }

  edit(user: any) {
  }
}
