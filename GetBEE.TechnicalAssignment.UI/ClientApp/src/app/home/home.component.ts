import { Component } from '@angular/core';
import { UserService } from '../../shared/services/user.service';
import { AuthenticationService } from '../../shared/services/authentication.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})

export class HomeComponent {

  user: any = null;
  userSaved: boolean= false;
  constructor(private userService: UserService,
    private authenticationService: AuthenticationService) {
    let userId = authenticationService.isLoggedIn ? authenticationService.getClaims() : "";
    if (userId != "") {
      this.userService
        .getUser(userId.sub)
        .subscribe(user => {
          this.user = user;
        });
    }
  }

  saveUser() {
    this.userSaved = false;
    this.userService
      .saveUser(this.user)
      .subscribe(user => {
        this.userSaved = true;
        this.user = user;
      }, error => {
          this.userSaved = false;
          console.error(error);
      });
  }
}
